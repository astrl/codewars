package algorithms

/*
Write a program that will calculate the number of trailing zeros in a factorial of a given number.
N! = 1 * 2 * 3 *  ... * N
Be careful 1000! has 2568 digits...

The solution: https://en.wikipedia.org/wiki/Factorial#Divisibility_and_digits
*/

import (
	"strconv"
)

func FactorialZeros(n int) int {
	return (n - getBaseFiveDigitSum(n)) / 4
}

func getBaseFiveDigitSum(n int) int {
	var strNumber string

	for n > 0 {
		remainder := n % 5
		n = n / 5
		strNumber = strconv.Itoa(remainder) + strNumber
	}

	var sum int
	for _, n := range []rune(strNumber) {
		d, err := strconv.ParseInt(string(n), 5, 32)
		if err != nil {
			panic(err)
		}

		sum += int(d)
	}

	return sum
}
