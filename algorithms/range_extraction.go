package algorithms

import (
	"fmt"
	"strconv"
	"strings"
)

/*
Complete the solution so that it takes a list of integers in increasing order and returns a correctly formatted string
in the range format:
RangeExtraction([-10, -9, -8, -6, -3, -2, -1, 0, 1, 3, 4, 5, 7, 8, 9, 10, 11, 14, 15, 17, 18, 19, 20]);
returns "-10--8,-6,-3-1,3-5,7-11,14,15,17-20"
*/

func RangeExtraction(list []int) string {
	var sequences [][]int
	var s []int
	for i := 0; i < len(list); i++ {
		seqLen := len(s)
		if seqLen == 0 || s[seqLen-1] == list[i]-1 {
			s = append(s, list[i])
			continue
		}

		sequences = append(sequences, s)
		s = []int{list[i]}
	}
	// accounting for the last sequence
	if len(s) != 0 {
		sequences = append(sequences, s)
	}

	var ranges []string
	for _, sequence := range sequences {
		seqLen := len(sequence)
		start := strconv.Itoa(sequence[0])
		if seqLen < 2 {
			ranges = append(ranges, start)
			continue
		}

		format := "%s,%s"
		if seqLen > 2 {
			format = "%s-%s"
		}

		end := strconv.Itoa(sequence[seqLen-1])
		ranges = append(ranges, fmt.Sprintf(format, start, end))
	}

	return strings.Join(ranges, ",")
}
