package algorithms

import (
	"strconv"
	"strings"
)

/*
Don't give me five!
In this kata you get the start number and the end number of a region and should return the count of all numbers except
numbers with a 5 in it. The start and the end number are both inclusive!
*/

func DontGiveMeFive(start int, end int) int {
	var cnt int
	for i := start; i <= end; i++ {
		if strings.Contains(strconv.Itoa(i), "5") {
			continue
		}
		cnt++
	}
	return cnt
}
