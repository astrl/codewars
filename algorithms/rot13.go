package algorithms

import (
	"strings"
)

/*
How can you tell an extrovert from an introvert at NSA? Va gur ryringbef, gur rkgebireg ybbxf ng gur BGURE thl'f fubrf.
I found this joke on USENET, but the punchline is scrambled. Maybe you can decipher it? According to Wikipedia, ROT13
(http://en.wikipedia.org/wiki/ROT13) is frequently used to obfuscate jokes on USENET.
Hint: For this task you're only supposed to substitue characters. Not spaces, punctuation, numbers etc.
*/

var rot13Rosetta = map[rune]rune{
	'a': 'n', 'n': 'a',
	'b': 'o', 'o': 'b',
	'c': 'p', 'p': 'c',
	'd': 'q', 'q': 'd',
	'e': 'r', 'r': 'e',
	'f': 's', 's': 'f',
	'g': 't', 't': 'g',
	'h': 'u', 'u': 'h',
	'i': 'v', 'v': 'i',
	'j': 'w', 'w': 'j',
	'k': 'x', 'x': 'k',
	'l': 'y', 'y': 'l',
	'm': 'z', 'z': 'm',
}

func Rot13(msg string) string {
	var out string

	for _, r := range []rune(msg) {
		if r13, ok := rot13Rosetta[r]; ok {
			out += string(r13)
			continue
		}

		if r13, ok := rot13Rosetta[runeToLowercase(r)]; ok {
			out += string(runeToUppercase(r13))
			continue
		}

		out += string(r)
	}

	return out
}

func runeToLowercase(r rune) rune {
	return rune(strings.ToLower(string(r))[0])
}

func runeToUppercase(r rune) rune {
	return rune(strings.ToUpper(string(r))[0])
}
