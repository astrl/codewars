package algorithms

import (
	"sort"
	"strings"
)

//Given two arrays of strings a1 and a2 return a sorted array r in lexicographical order of the strings of a1 which are
//substrings of strings of a2.
//Example 1:
//a1 = ["arp", "live", "strong"]
//a2 = ["lively", "alive", "harp", "sharp", "armstrong"]
//returns ["arp", "live", "strong"]
//Example 2:
//a1 = ["tarp", "mice", "bull"]
//a2 = ["lively", "alive", "harp", "sharp", "armstrong"]
//returns []

func InArray(array1 []string, array2 []string) []string {
	outMap := make(map[string]struct{})

	for _, substring := range array1 {
		var found bool
		for _, word := range array2 {
			if strings.Contains(word, substring) {
				found = true
				break
			}
		}

		if found {
			outMap[substring] = struct{}{}
		}
	}

	out := make([]string, 0, len(outMap))
	for key := range outMap {
		out = append(out, key)
	}

	sort.Strings(out)

	return out
}
