package algorithms

/* Implement a function that receives two IPv4 addresses, and returns the number of addresses between them (including the
first one, excluding the last one).All inputs will be valid IPv4 addresses in the form of strings. The last address will
always be greater than the first one.
*/

import (
	"math"
	"strconv"
	"strings"
)

func IpsInRange(start, end string) int {
	return int(numberOfTakenIps(end) - numberOfTakenIps(start))
}

func numberOfTakenIps(ip string) uint {
	var taken uint
	octets := strings.Split(ip, ".")

	for i := 0; i < 4; i++ {
		val, _ := strconv.ParseUint(octets[i], 10, 32)
		taken += uint(val) * uint(math.Pow(256, float64(3-i)))
	}

	return taken
}
