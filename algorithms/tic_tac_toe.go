package algorithms

/*
If we were to set up a Tic-Tac-Toe game, we would want to know whether the board's current state is solved, wouldn't we?
Our goal is to create a function that will check that for us!
Assume that the board comes in the form of a 3x3 array, where the value is 0 if a spot is empty, 1 if it is an "X",
or 2 if it is an "O".
We want our function to return:
1) -1 if the board is not yet finished AND no one has won yet (there are empty spots),
2) 1 if "X" won,
3) 2 if "O" won,
4) 0 if it's a cat's game (i.e. a draw).
*/

func IsSolved(board [3][3]int) int {
	for i := 0; i < 3; i++ {
		// check row
		if rowWinner, ok := directionWinner(board[i][0], board[i][1], board[i][2]); ok {
			return rowWinner
		}

		// check column
		if colWinner, ok := directionWinner(board[0][i], board[1][i], board[2][i]); ok {
			return colWinner
		}
	}

	// check skewed paths
	if skewWinner, ok := directionWinner(board[0][0], board[1][1], board[2][2]); ok {
		return skewWinner
	}

	if skewWinner, ok := directionWinner(board[0][2], board[1][1], board[2][0]); ok {
		return skewWinner
	}

	// game is not yet finished
	for i := 0; i < 3; i++ {
		for j := 0; j < 3; j++ {
			if board[i][j] == 0 {
				return -1
			}
		}
	}

	// draw
	return 0
}

// directionWinner checks for direction winner.
// Returns true when winner is either O or X.
func directionWinner(vs ...int) (int, bool) {
	if len(vs) == 0 {
		return 0, false
	}

	control := vs[0]
	if control == 0 {
		return 0, false
	}

	for _, v := range vs {
		if v != control {
			return 0, false
		}
	}

	return control, true
}
