package algorithms

/*Find the missing letter

Write a method that takes an array of consecutive (increasing) letters as input and that returns the missing letter in
the array. You will always get an valid array. And it will be always exactly one letter be missing. The length of the
array will always be at least 2. The array will always contain letters in only one case.
Example:
['a','b','c','d','f'] -> 'e' ['O','Q','R','S'] -> 'P'
*/

func FindMissingLetter(chars []rune) rune {
	bytes := []byte(string(chars))

	var missingLetter rune
	for i := 0; i < len(bytes)-1; i++ {
		if bytes[i]+1 != bytes[i+1] {
			missingLetter = rune(bytes[i] + 1)
		}
	}

	return missingLetter
}
