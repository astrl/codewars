package algorithms

//https://www.codewars.com/kata/550f22f4d758534c1100025a/
// Once upon a time, on a way through the old wild mountainous west,… a man was given directions to go from one point to
// another. The directions were "NORTH", "SOUTH", "WEST", "EAST". Clearly "NORTH" and "SOUTH" are opposite, "WEST" and
//"EAST" too. Going to one direction and coming back the opposite direction right away is a needless effort. Since this
//is the wild west, with dreadful weather and not much water, it's important to save yourself some energy, otherwise
//you might die of thirst!

var opposites = map[string]string{
	"NORTH": "SOUTH",
	"SOUTH": "NORTH",
	"WEST":  "EAST",
	"EAST":  "WEST",
}

func DirReduc(arr []string) []string {
	reduced := newStack()

	for _, dir := range arr {
		prevDir := reduced.Peek()
		if prevDir != "" && opposites[prevDir] == dir {
			reduced.Pop()
			continue
		}

		reduced.Push(dir)
	}

	return reduced.ToSlice()
}

type stack struct {
	data []string
}

func newStack() *stack {
	return &stack{
		data: make([]string, 0),
	}
}

func (s *stack) Peek() string {
	n := len(s.data)
	if n == 0 {
		return ""
	}

	return s.data[n-1]
}

func (s *stack) Push(val string) {
	s.data = append(s.data, val)
}

func (s *stack) Pop() string {
	n := len(s.data)
	if n == 0 {
		return ""
	}

	var val string
	val, s.data = s.data[n-1], s.data[:n-1]

	return val
}

func (s *stack) ToSlice() []string {
	return s.data
}
